// 宣言と同時に代入もできる
let letVariable = "Riiiver";

let letVariable; // 再宣言ができない(※できる時もあります)

letVariable = "iiidea"; // 再代入はできます
console.log(letVariable);

const constVariable = "Piece";

const constVariable; // 再宣言も再代入もできない
constVariable = "おぼえられました？";
console.log(constVariable);