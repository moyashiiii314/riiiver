// request-promiseを読み込みます
const requestPromise = require("request-promise");

// GASから支給されたURLを設定します
const api = "https://script.google.com/macros/s/xxxxxx/exec";

// 送信するデータを設定
const sendData = "test";

// URL作成 URL + GAS で設定した値
const url = `${api}?data=${sendData}`;
console.log(url);

// 以降がAPI通信の処理となります
// API通信を行い、成功したら result を、失敗したら error message を『console.log』で出力します
// sendGasDatas関数の中でtry...catchでエラー回避を行っています
const sendGasData = async () => {
  try {
    const result = await requestPromise({
      method: "GET",
      uri: url
  })

    return console.log(result);

  } catch (error) {
    console.log("Error message: " + error.message);
  }
}

sendGasData();