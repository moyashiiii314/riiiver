// JSONを読み込む
var jsonList = require("./json05.json");

// for文(=繰り返し) で JSONデータ を1つずつ出力
for ( var data in jsonList) { // data が keyword の代わりになる
    console.log(data); // JSONデータの keyword が出力されているのを確認
    console.log(jsonList[data]) //keyword に対応するデータを出力
}