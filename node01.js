// request-promiseを読み込みます
const requestPromise = require("request-promise");

// 使用するAPIサービスのURLを設定します
const api = "http://zipcloud.ibsnet.co.jp/api/search";

// 郵便番号を設定
const postalCode = xxxxxxx;

// URL作成 URL + 郵便番号 console.log()は確認用
const url = `${api}?zipcode=${postalCode}`;
console.log(url);

// 以降がAPI通信の処理
// API通信を行い、成功したら result を、失敗したら error message を『console.log』で出力します
// getAddres関数の中でtry...catchでエラー回避を行っています
const getAddres = async () => {
  try {
    const result = await requestPromise({ // JSON形式なので注意！
      method: "GET", // GETなのかPOSTなのかを指定
      uri: url // 使用するURLを指定
    })

    return console.log(result);

  } catch (error) {
    console.log("Error message: " + error.message);
  }  
}

getAddres();
  
