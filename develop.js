// 演習問題に挑戦！

// 問題1：
// 引数が「”Riiiver”」であれば「成功!」と出力して、そうではない場合は「失敗!」を出力する関数を作ってみよう
// ※回答では、以下の変数を使用しているので合わせると答え合わせが分かりやすい
// 関数 : developerFunction,  引数 : text

// 問題2：
// 以下のコードはエラーが起きているので直してみよう。

try {
    var iiidea = "T+S+A";

    if iiidea === "T+S+A" {
        consol.log("そうです！ iiidea = T+S+Aです！");
    } else {
        console.log("iiideaが何なのか復習しよう!!");

} catch(error) {
    console.log(error);
    console.log("まだエラーがあります。エラー内容を見てどこを直せばいいか確認しよう");
}