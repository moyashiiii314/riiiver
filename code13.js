// for文(=繰り返し) で 配列 を1つずつ出力
// for ( 変数の宣言と代入; 繰り返しが続く条件; 変数の増やし方/減らし方) {
//    繰り返す処理
// }

var list = ["Riiiver", "iiidea", "Piece"];

// index という変数を宣言をして、for文で使う
// index が listのデータ数より大きくなったら for文は終わり
// ++ は「1ずつ増える」という意味
for ( var index = 0; index < list.length; index ++) {
    console.log(list[index]); // 1回目は index = 0, 2回目は index = 1, 3回目は index = 2
}