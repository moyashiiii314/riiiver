// request-promiseを読み込みます
const requestPromise = require("request-promise");

// openweathermapのURLとAPIキーを設定
const api = "http://api.openweathermap.org/data/2.5/weather";
const apiKey = "f3c00053bff6503205042aef63ad7797";

// 送信するデータを設定 ここでは都市名
const cityName = "tokyo";

// URL作成 URL + APIキー + 都市名 (ついでに℃になるように設定)
const url = `${api}?APPID=${apiKey}&q=${cityName}&units=metric`;
console.log(url);

// 以降がAPI通信の処理となります

// 後から必要な情報を追加するために空のデータを作成
const weatherData = {}

const openWeatherMap = async () => {
    try {
      const result = await requestPromise({
        method: "GET",
        uri: url
      })
    　// JavaScriptで扱えるようにする(おまじない)   
      let parseResult = JSON.parse(result)

    　// 必要な情報を抜き出す 華氏を摂氏に変更   
      const temp = parseResult["main"]["temp"];
      const weather = parseResult["weather"][0]["main"];

    　// 欲しいデータだけを空のデータに追加 JSON形式   
      weatherData["temp"] = temp
      weatherData["weather"] = weather

      return console.log(weatherData)
  
    } catch (error) {
      console.log("Error message: " + error.message);
    }
}

openWeatherMap();