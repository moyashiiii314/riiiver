// if (条件 : true or false を確認) {
//  true : 正しい時 に実行される
// } else {
//  false : 正しくない時 に実行される
// }

var riiiver = "riiiver";
if (riiiver === "riiiver") { // 「===」は「左辺」と「右辺」が同じという意味
    console.log("riiiverをよろしくお願いします！");
} else {
    // var riiiver = "river" にするとこっちが実行される
    console.log("riiiverじゃありませんね!?"); 
}