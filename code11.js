// JSONを読み込む
var jsonList = require("./json04.json");

// 変数に渡して出力
var userNameList = jsonList.userNameList;
console.log(userNameList);

// インデックスを指定することもできる
console.log(userNameList[0]);

// データの数も調べられる
console.log(userNameList.length);