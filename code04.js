// 文字と文字は「+」で繋げられる
console.log("私は" + "Riiiverです。");

// 変数に代入してても大丈夫
var here = "こちらは";
var jellyfish = "クラゲ";
console.log(here + jellyfish + "です。");

// 「+」を使わない書き方
var who = "だれ？";
console.log(`あなたは${who}`);