console.log("Hello");
console.log("ここの文字が出力されています！");

// 「//」がコメントの目印です。コメントは実行されません。
// console.log("コメントだから実行されない");
// console.log("// を外すと実行される");

console.log("文字はシングルクォートかダブルクォートで囲む");

// 数字はクォート不要
console.log(1);