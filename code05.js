console.log(3 + 4); // 足し算：+
console.log(9 - 2); // 引き算：-
console.log(7 * 1); // 掛け算：*
console.log(14 / 2); // 割り算：/
console.log(15 % 8); // 余り: %

// 変数に代入もできる
var result = 30 + 47;
console.log(result);

// これで変数名を変更できずに値を変えられる
result = result + 700;
console.log(result);