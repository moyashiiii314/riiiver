// 問題1の回答
var developerFunction = (text) => {
    // if文でtextがRiiiverかどうかをチェック
    if ( text === "Riiiver") {
        console.log("成功!")
    } else {
        console.log("失敗!")
    }
};

developerFunction("Riiiver");

// 問題2の回答
try {
    var iiidea = "T+S+A";

    if (iiidea === "T+S+A") {
        console.log("そうです！ iiidea = T+S+Aです！");
    } else {
        console.log("iiideaが何なのか復習しよう!!");
    }   
} catch(error) {
    console.log(error);
    console.log("まだエラーがあります。エラー内容を見てどこを直せばいいか確認しよう");
}