// request-promiseを読み込みます
const requestPromise = require("request-promise");

// livedoor,gasのURL
const api = "http://weather.livedoor.com/forecast/webservice/json/v1";
const gasApi = "https://script.google.com/macros/s/AKfycbz_0dOebqTb5p03Mzxcwn74BpmHWzMnau6CwVZIr7dIU1YCiP8_/exec";

// 送信するデータを設定 ここでは東京のid
const cityID = "130010";

// URL作成 URL + APIキー + 都市名
const url = `${api}?city=${cityID}`;
console.log(url);

// 以降がAPI通信の処理となります
// API通信を行い、成功したら result を、失敗したら error message を『console.log』で出力します

const weatherData = {};

const getWeatherData = async () => {
    try {
        const result = await requestPromise({
            method: "GET",
            uri: url
        })
        let parseResult = JSON.parse(result);
    
        const todayWeather = parseResult["forecasts"][0]["telop"];
        const todayHighestTemperature= parseResult["forecasts"][0]["temperature"]["max"]["celsius"];
        weatherData["todayWeather"] = todayWeather
        weatherData["todayHighestTemperature"] = todayHighestTemperature;     
        return weatherData
    } catch (error) {
        console.log("Error message: " + error.message);
    }
}

const sendGoogleSpreadSheet = async () => {
    const sendData = await getWeatherData();
    console.log(sendData);
    try {
      const gasResult = await requestPromise({
        method: "GET",
        uri: gasApi,
        qs: sendData
      })
  
      return gasResult;
    } catch (error) {
        console.log("Error message: " + error.message);
    };
  }
  
  sendGoogleSpreadSheet();