function doGet(e){
    // URLからデータを取得     
    var getParameter = e.parameter.data // data のところは自由に変えられる

    // 現状Activeになっているsheetを取得
    var sheet = SpreadsheetApp.getActiveSheet();
 
    // シートに取得したデータを追記
    sheet.appendRow([getParameter]);
}