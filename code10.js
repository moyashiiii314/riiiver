// require(JSONファイルの場所)  JSONファイルを読み込む
var jsonData = require("./json03.json");

// console.log() で出力できる
console.log(jsonData);

// keyword を指定することもできる
console.log(jsonData["prefecturalCapital"]);

// {} の中に {} がさらにある場合でも大丈夫
console.log(jsonData["prefecturalCapital"]["name"]);

// 上書きもできる ※元データは上書きされない
jsonData["name"] = "Saitama";
console.log(jsonData["name"]);