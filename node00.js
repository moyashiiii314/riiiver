// 解説はしません

// モジュールをインポート
var http = require('http');

// インポートしたモジュールを使ってサーバーを立てる
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello Node.js');
}).listen(8989, '127.0.0.1');