// request-promiseを読み込みます
const requestPromise = require("request-promise");

// openweathermapのURLを設定
const api = "http://api.openweathermap.org/data/2.5/weather";

// 送信するデータを設定 ここではAPIキーと都市名
const apiKey = "xxxxxxxxxxxxxxxxxx";
const cityName = "tokyo";

// URL作成 URL + APIキー + 都市名
const url = `${api}?APPID=${apiKey}&q=${cityName}`;
console.log(url);

// 以降がAPI通信の処理となります
// API通信を行い、成功したら result を、失敗したら error message を『console.log』で出力します
// openWeatherMap関数の中でtry...catchでエラー回避を行っています
const openWeatherMap = async () => {
  try {
    const result = await requestPromise({
      method: "GET",
      uri: url
    })
    
    return console.log(result);

  } catch (error) {
    console.log("Error message: " + error.message);
  }
}
      
openWeatherMap();
    