const prefectureIDList = {
    "北海道": 2130037,
    "青森": 2130656,
    "岩手": 2112518,
    "宮城": 2111888,
    "秋田": 2113124,
    "山形": 2110554,
    "福島": 2112922,
    "茨城": 2112669,
    "栃木": 1850310,
    "群馬": 1863501,
    "埼玉": 1853226,
    "千葉": 2113014,
    "東京": 1850147,
    "神奈川": 1860291,
    "新潟": 1855429,
    "富山": 1849872,
    "石川": 1861387,
    "福井": 1863983,
    "山梨": 1848649,
    "長野": 1856210,
    "岐阜": 1863640,
    "静岡": 1851715,
    "愛知": 1865694,
    "三重": 1857352,
    "滋賀": 1852553,
    "京都": 1857910,
    "大阪": 1853908,
    "兵庫": 1862047,
    "奈良": 1855608,
    "和歌山": 1848938,
    "鳥取": 1849892,
    "島根": 1852442,
    "岡山": 1854381,
    "広島": 1862413,
    "山口": 1848681,
    "徳島": 1850157,
    "香川": 1860834,
    "愛媛": 1864226,
    "高知": 1859133,
    "福岡": 1863958,
    "佐賀": 1853299,
    "長崎": 1856156,
    "熊本": 1858419,
    "大分": 1854484,
    "宮崎": 1856710,
    "鹿児島": 1860825,
    "沖縄": 1894616
}

exports.handler = async (event) => {
  // 受け取ったデータから県名を設定
  const prefecture = event.properties.preferences.prefecture;

  // 県名からIDを取得
  const prefectureID = prefectureIDList[prefecture]

  // request-promiseを読み込みます
  const requestPromise = require("request-promise");

  // 使用するAPIサービスのURL/APIkeyを設定 openwethaermapとgas
  const api = "http://api.openweathermap.org/data/2.5/weather";
  const gasApi = event.properties.preferences.gasURL;
  const apiKey = event.properties.preferences.opernWeatherMapAPIKey

  // URL作成 URL + APIキー + 都市ID (ついでに℃になるように設定)
  const url = `${api}?APPID=${apiKey}&id=${prefectureID}&units=metric`;

  // 以降がAPI通信の処理となります
  const weatherData = {} // 後から必要な情報を追加するために空のデータを作成

  const openWeatherMap = async () => {
    try {
      const result = await requestPromise({
        method: "GET",
        uri: url
      })
    　   
      let parseResult = JSON.parse(result) // JavaScriptで扱えるようにする(おまじない)

      const temp = parseResult["main"]["temp"]; // 必要な情報を抜き出す
      const weather = parseResult["weather"][0]["main"]; // 必要な情報を抜き出す

      weatherData["temp"] = temp // 欲しいデータを空のデータに追加 JSON形式 
      weatherData["weather"] = weather // 欲しいデータを空のデータに追加 JSON形式 

      return weatherData
  
    } catch (error) {
      console.log("Error message: " + error.message);
    }
}

const sendGasData = async () => {
    const sendData = await openWeatherMap();
    console.log(sendData);
    try {
      const gasResult = await requestPromise({
        method: "GET",
        uri: gasApi,
        qs: sendData
    })
  
    return gasResult;
  
    } catch (error) {
      console.log("Error message: " + error.message);
    }
  }
  
  sendGasData();
}