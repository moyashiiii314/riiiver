// アロー関数を作成
var testFunction = () => {
    console.log("testFunction();で実行");
}
testFunction(); // ここで実行

var testFunction = (num) => { // num が引数
    var result = num * 2　// ここが処理(引数 × 2)
    return result // result が戻り値
}

// 引数に4を渡している この場合、num = 4 となっている
var returnNum = testFunction(4);  // returnは出力をしないので注意
console.log(returnNum); // 戻り値を出力