// request-promiseを読み込みます
const requestPromise = require("request-promise");

// openweathermapのURLとAPIキーとSpread SheetのURLを設定
const api = "http://api.openweathermap.org/data/2.5/weather";
const apiKey = "f3c00053bff6503205042aef63ad7797";
const gasApi = "https://script.google.com/macros/s/AKfycbz_0dOebqTb5p03Mzxcwn74BpmHWzMnau6CwVZIr7dIU1YCiP8_/exec";

// 送信するデータを設定 ここでは都市名
const cityName = "tokyo";

// URL作成 URL + APIキー + 都市名 (ついでに℃になるように設定)
const url = `${api}?APPID=${apiKey}&q=${cityName}&units=metric`;
console.log(url);

// 以降がAPI通信の処理となります

// 後から必要な情報を追加するために空のデータを作成
const weatherData = {}

const openWeatherMap = async () => {
    try {
      const result = await requestPromise({
        method: "GET",
        uri: url
      })
    　// JavaScriptで扱えるようにする(おまじない)   
      let parseResult = JSON.parse(result)

    　// 必要な情報を抜き出す
      const temp = parseResult["main"]["temp"];
      const weather = parseResult["weather"][0]["main"];

    　// 欲しいデータだけを空のデータに追加 JSON形式   
      weatherData["temp"] = temp
      weatherData["weather"] = weather

      return weatherData
  
    } catch (error) {
      console.log("Error message: " + error.message);
    }
}

const sendGasData = async () => {
    const sendData = await openWeatherMap();
    console.log(sendData);
    try {
      const gasResult = await requestPromise({
        method: "GET",
        uri: gasApi,
        qs: sendData
    })
  
    return gasResult;
  
    } catch (error) {
      console.log("Error message: " + error.message);
    }
  }
  
  sendGasData();